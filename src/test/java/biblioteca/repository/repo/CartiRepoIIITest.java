package biblioteca.repository.repo;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoIIITest {
    CartiRepoInterface repo;

    @Before
    public void setUp() throws Exception {
         repo=new CartiRepo();
    }

    @Test
    public void getCartiOrdonateDinAnulPassed() throws Exception {
        List<Carte> cartiOrdonate=repo.getCartiOrdonateDinAnul("1948");
        assertEquals(3,cartiOrdonate.size());
        Carte expected= repo.cautaCarte("Caragiale Ion").get(0);
        assertEquals(expected.getTitlu(),cartiOrdonate.get(0).getTitlu());
    }
    @Test
    public void getCartiOrdonateDinAnulFailed() throws Exception {
        List<Carte> cartiOrdonate=repo.getCartiOrdonateDinAnul("2000");
        assertEquals(0,cartiOrdonate.size());

    }

}