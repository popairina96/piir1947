package biblioteca.repository.repo;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import biblioteca.repository.repoMock.CartiRepoMock2;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoTest {
    CartiRepoInterface repo;
    BibliotecaCtrl ctrl;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void cautaCarteInListaVida() throws Exception {
        repo=new CartiRepoMock2();
        String author="ion";
        List<Carte> rezultat;
        rezultat=repo.cautaCarte(author);
        assertEquals(rezultat.size(),0);
    }

    @Test
    public void cautaCarteSiGaseste() throws Exception {
        repo=new CartiRepoMock();
        String author="ion";
        List<Carte> rezultat;
        rezultat=repo.cautaCarte(author);
        assertEquals(rezultat.size(),2);
    }

    @Test
    public void cautaCarteSiNuGaseste() throws Exception {

        repo=new CartiRepoMock();
        String author="andrei";
//        System.out.println(repo.getCarti());
        List<Carte> rezultat;
        rezultat=repo.cautaCarte(author);
        assertEquals(rezultat.size(),0);
    }
    @Test
    public void cautaCarteInvalid() throws  Exception{
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Autor invalid!");
        repo=new CartiRepoMock();
        ctrl=new BibliotecaCtrl(repo);
        String author="demonstrativă pentru li6terele respective. Nu doar că a supravieţuit timp de cinci secole, dar şi a facut saltul în tipografia electronică practic neschimbată. A fost popularizată în anii '60 odată cu ieşirea colilor Letraset care conţineau pasaje Lorem Ipsum, iar mai recent, prin programele de publicare pentru calculator, ca Aldus PageMaker care includeau versiuni de Lorem IpsumLorem Ipsum este pur şi simplu o machetă pentru text a industriei tipografice. Lorem Ipsum a fost macheta standard a industriei încă din secolul al XVI-lea, când un tipograf anonim a luat o planşetă de litere şi le-a amestecat pentru a crea o carte demonstrativă pentru literele respective. Nu doar că a supravieţuit timp de cinci secole, dar şi a facut saltul în tipografia electronică practic neschimbată. A fost popularizată în anii '60 odată cu ieşirea colilor Letraset care conţineau pasaje Lorem Ipsum, iar mai recent, prin programele de publicare pentru calculator, ca Aldus PageMaker care includeau versiuni de Lorem Ipsum.Lorem Ipsum este pur şi simplu o machetă pentru text a industriei tipografice. Lorem Ipsum a fost macheta standard a industriei încă din secolul al XVI-lea, când un tipograf anonim a luat o planşetă de litere şi le-a amestecat pentru a crea o carte ";
        List<Carte> rezultat;
        rezultat=ctrl.cautaCarte(author);
        System.out.println(rezultat);
    }

}