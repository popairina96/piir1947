package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {
    CartiRepoInterface repo;
    BibliotecaCtrl ctr;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp(){
        this.repo=new CartiRepo();
        this.ctr=new BibliotecaCtrl(repo);
    }
    @Test
    public void ECP1_CarteValida(){
        //date valide
        int dimensiuneFisierInainte = repo.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Alexandre Dumas");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("conte");
        cuvinteCheie.add("inchisoare");
        cuvinteCheie.add("drama");
        Carte carte = new Carte();
        carte.setTitlu("Contele de Monte Cristo");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1844");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Casa veche");
        try {
            ctr.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = repo.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }
    @Test
    public void ECP4_CarteInvalida() throws Exception{
        //an aparitie < 1000
        expectedException.expect(Exception.class);
        expectedException.expectMessage("An invalid!");
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Alexandre Dumas");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("conte");
        cuvinteCheie.add("inchisoare");
        cuvinteCheie.add("drama");
        Carte carte = new Carte();
        carte.setTitlu("Contele de Monte Cristo");
        carte.setReferenti(referenti);
        carte.setAnAparitie("omie");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Casa veche");
        ctr.adaugaCarte(carte);

    }
    @Test
    public void BVA3_CarteValida() throws Exception{
        int dimensiuneFisierInainte = repo.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("A");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("conte");
        cuvinteCheie.add("inchisoare");
        cuvinteCheie.add("drama");
        Carte carte = new Carte();
        carte.setTitlu("Contele de Monte Cristo");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1844");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Casa veche");
        try {
            ctr.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = repo.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);

    }
    @Test
    public void BVA6_CarteInvalida() throws Exception{
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Titlu invalid!");
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Alexandre Dumas");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("conte");
        cuvinteCheie.add("inchisoare");
        cuvinteCheie.add("drama");
        Carte carte = new Carte();
        carte.setTitlu("Contele de Monte Cristo Contele de Monte Cristo Contele de Monte Cristo Contele de Monte Cristo Contele de Monte Cristo Contele de Monte Cristo Contele de Monte Cristo Contele de Monte Cristo Contele de Monte Cristo Contele de Monte Cristo Contele de Monte 1");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1844");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Casa veche");
        ctr.adaugaCarte(carte);
    }

    @Test
    public void TC1_ECP() {
        //date valide
        int dimensiuneFisierInainte = repo.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("Titlu");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Ed");
        try {
            ctr.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = repo.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void TC2_ECP() throws Exception{
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Titlu invalid!");
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("22");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Edit");
        ctr.adaugaCarte(carte);

    }
    @Test
    public void TC3_BVA() throws Exception{
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Titlu invalid!");
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("c");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("edit");
        ctr.adaugaCarte(carte);

    }
    @Test
    public void TC3_ECP() throws Exception{
        //an aparitie < 1000
        expectedException.expect(Exception.class);
        expectedException.expectMessage("An invalid!");
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("c");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("Titlu");
        carte.setReferenti(referenti);
        carte.setAnAparitie("sds");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Ed");
        ctr.adaugaCarte(carte);

    }
    @Test
    public void TC4_ECP() {
        int dimensiuneFisierInainte = repo.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("Titlu");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1989");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Ed");
        try {
            ctr.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = repo.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void TC1_BVA() {
        //titlu contine o litera
        int dimensiuneFisierInainte = repo.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("T");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("edit");
        try {
            ctr.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = repo.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }
    @Test
    public void TC2_BVA() {
        //lista de referenti e vida
        int dimensiuneFisierInainte = repo.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        //  referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("T");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("edit");
        try {
            ctr.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = repo.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }
    @Test
    public void TC4_BVA() {
        //editura contine o litera
        int dimensiuneFisierInainte = repo.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("T");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("E");
        try {
            ctr.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = repo.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void TC5_BVA() {
        //editura = ""
        int dimensiuneFisierInainte = repo.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("T");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        //   carte.setEditura("E");
        try {
            ctr.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = repo.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }
    @Test
    public void TC7_BVA() {
        //editura = ""
        int dimensiuneFisierInainte = repo.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("T");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        //   carte.setEditura("E");
        try {
            ctr.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = repo.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }
}