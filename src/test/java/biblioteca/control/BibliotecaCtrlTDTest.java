package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BibliotecaCtrlTDTest {
    CartiRepoInterface repo;
    BibliotecaCtrl ctr;
    @Before
    public void setUp() throws Exception {
        this.repo=new CartiRepo();
        this.ctr=new BibliotecaCtrl(repo);
    }

    @Test
    public void testUnitarA() throws Exception {
        //pt adaugare
        int dimensiuneFisierInainte = repo.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Liviu Rebreanu");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("pamant");
        cuvinteCheie.add("drama");
        Carte carte = new Carte();
        carte.setTitlu("Ion");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1920");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Editura Oarecare");
        try {
            ctr.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = repo.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void testIntegrareB() throws Exception {
        testUnitarA();
        String author="ion";
        List<Carte> rezultat;
        rezultat=repo.cautaCarte(author);
        assertNotEquals(rezultat.size(),0);
    }

    @Test
    public void testIntegrareC() throws Exception {

        List<Carte> cartiOrdonate=repo.getCartiOrdonateDinAnul("1948");
        assertEquals(3,cartiOrdonate.size());
        Carte expected= repo.cautaCarte("Caragiale Ion").get(0);
        assertEquals(expected.getTitlu(),cartiOrdonate.get(0).getTitlu());
        testIntegrareB();
    }



}